package main

import "time"

type ranGen struct {
	CurrentLuckyNum int `json:"current_lucky_num"`
	LuckyNumHistory []struct {
		LuckyNum  int       `json:"lucky_num"`
		VisitedAt time.Time `json:"visited_at"`
	} `json:"lucky_num_history"`
}
