package main

import (
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"time"
)

func main() {
	http.HandleFunc("/api/lucky", ranGenHandler)

	fmt.Println("starting HTTP server")
	log.Fatal(http.ListenAndServe(":8080", nil))
}

func ranGenHandler(w http.ResponseWriter, r *http.Request) {

	random := ranGen{}

	num, err := json.Marshal(random)
	if err != nil {
		panic(err)
	}

	w.Write(num)

}

func roulette(a int) int {

	rand.Seed(time.Now().UnixNano())
	fmt.Println(rand.Intn(30))

	return a
}
